/*
Copyright 2013 Patrick Borgeest

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

/*

	This file is specifically for my own use. I do not anticipate that
	anybody will use the executable.

	Example usage:
		In the current directory there are a set of files:
			[Base.png, Base 1.png, Base 2.png, Base 3.png, Base 4.png]
		Base.png is empty
		Base 1.png contains the front of page one
		Base 2.png contains the front of page two
		Base 3.png contains the back of page two
		Base 4.png contains the back of page one
		This tool called as './nmtcpile Base.png 4' and will change the
		filenames of each file so that the back of each page is ordered
		numerically immediately after the front of each page. This tool
		then uploads each file to the server described in the config

		Note the order of pages in the original files. they are created by
		taking the pile of pages with page one on the top and face up and 
		putting them in the automatic feed tray of the scanner and running
		the pile through the scanner.  Pick up the pile and turn it over
		and put it in the automatic feed tray so that the back of the last
		page is face up and run the pile through the scanner.

		My cheap-ass scanner only does one side of the paper at a time which
		is the whole reason for this tool.

		Image Capture on the Mac is responsible for numbering the files in
		that stupid fashion, by the way.

*/

import (
	"bitbucket.org/pborgeest/nometicland/tools/common"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var (
	flagDev      = flag.Bool("dev", false, "Use 'Development' server config")
	flagConfig   = flag.String("config", ".nometic.gcfg", "Filename of the config file")
	flagBasename = flag.Bool("withbasename", false, "Include the basename file in the uploads")
)

func main() {
	flag.Parse()
	basename, maxnumber := getArgs()

	server, err := common.GetServerFromConfig(*flagConfig, *flagDev)
	if err != nil {
		log.Fatal(err)
	}

	baseprefix, basesuffix := splitBasename(basename)
	renumber(baseprefix, basesuffix, maxnumber)

	for i := 1; i <= maxnumber; i++ {
		uploadThisFile := fmt.Sprintf("%s%d%s", baseprefix, i, basesuffix)
		if err := common.OneFile(uploadThisFile, server); err != nil {
			log.Fatal(err)
		}
	}
}

func renumber(baseprefix, basesuffix string, maxnumber int) {
	if *flagBasename {
		// preprocess the files increasing the indices by one and renaming the basefile
		// nb must loop through indices from the highest number to avoid clobbering files
		// nb alse that maxnumber has already been modified to equal the maxnumber AFTER
		// this munging has been done (see getArgs())
		for i := maxnumber + 1; i > 1; i-- {
			renameFromIndexToNewIndex(baseprefix, basesuffix, i-1, i)
		}
		originalName := fmt.Sprintf("%s%s", baseprefix, basesuffix)
		newName := fmt.Sprintf("%s %d%s", baseprefix, 1, basesuffix)
		os.Rename(originalName, newName)
	}

	halfmax := maxnumber / 2

	// move the front pages 
	for i := 1; i <= halfmax; i++ {
		newindex := i*2 - 1
		renameFromIndexToNewIndexAndLoseTheSpaceCharacter(baseprefix, basesuffix, i, newindex)
	}

	// move the back pages
	for i := halfmax + 1; i <= maxnumber; i++ {
		newindex := (1 + maxnumber - i) * 2
		renameFromIndexToNewIndexAndLoseTheSpaceCharacter(baseprefix, basesuffix, i, newindex)
	}
}

func renameFromIndexToNewIndex(baseprefix, basesuffix string, oldindex, newindex int) {
	renameFromIndexToIndexWithFormatString(baseprefix, basesuffix, oldindex, newindex, "%s %d%s")
}
func renameFromIndexToNewIndexAndLoseTheSpaceCharacter(baseprefix, basesuffix string, oldindex, newindex int) {
	renameFromIndexToIndexWithFormatString(baseprefix, basesuffix, oldindex, newindex, "%s%d%s")
}
func renameFromIndexToIndexWithFormatString(baseprefix, basesuffix string, oldindex, newindex int, format string) {
	originalName := fmt.Sprintf("%s %d%s", baseprefix, oldindex, basesuffix)
	newName := fmt.Sprintf(format, baseprefix, newindex, basesuffix)
	os.Rename(originalName, newName)
}

// examples:
// splitBasename("example.gif") -> ("example", ".gif")
// splitBasename("example.thing.gif") -> ("example.thing", ".gif")
func splitBasename(basename string) (string, string) {
	basedotindex := strings.LastIndex(basename, ".")
	return basename[:basedotindex], basename[basedotindex:]
}

func getArgs() (string, int) {
	if flag.NArg() < 2 {
		log.Fatalf("This tool must be called as '%s basename maxnumber'", os.Args[0])
	}

	if _, err := os.Open(flag.Arg(0)); err != nil {
		log.Fatal("This tool must be called with basename pointing to an existing file")
	}

	maxnumber, err := strconv.Atoi(flag.Arg(1))
	if *flagBasename {
		maxnumber = maxnumber + 1
	}

	if err != nil {
		log.Fatalf("This tool must be called as '%s basename maxnumber'", os.Args[0])
	}

	if maxnumber%2 != 0 {
		if *flagBasename {
			log.Fatal("This tool must be called with an odd maxnumber when using --withbasename")
		} else {
			log.Fatal("This tool must be called with an even maxnumber")
		}
	}

	return flag.Arg(0), maxnumber
}
