/*
Copyright 2013 Patrick Borgeest

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

// This tool takes the configuration described in the config file
// and sends the file or files named in the arguments to
// the server that the configuration tells it to.

/*
Example config file:

[Production]
Email=test@example.com
Password=pass
UploadUrl=http://example.appspot.com/uploadurl

[Development]
Email=test@example.com
Password=pass
UploadUrl=http://localhost:8080/uploadurl

*/

import (
	"bitbucket.org/pborgeest/nometicland/tools/common"
	"flag"
	"log"
)

var (
	flagDev    = flag.Bool("dev", false, "Use 'Development' server config")
	flagConfig = flag.String("config", ".nometic.gcfg", "Filename of the config file")
)

func main() {

	flag.Parse()

	server, err := common.GetServerFromConfig(*flagConfig, *flagDev)
	if err != nil {
		log.Fatal(err)
	}

	if flag.NArg() == 0 {
		log.Fatal("No files named on the command line")
	}

	for _, filename := range flag.Args() {
		if err := common.OneFile(filename, server); err != nil {
			log.Fatal(err)
		}
	}

}
