/*
Copyright 2013 Patrick Borgeest

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package upload provides a method that uploads a single file to a nometic server
package common

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
)

// Server tells OnePost where to send the file and how to log in there
type Server struct {
	// The email address to log in as
	UserEmail string
	// The password to log in with
	UserPassword string
	// the URL of the uploadurl resource on the webapp that returns
	// the generated URL where the client can POST to the blobstore.
	// For example, this will most likely be http://localhost:8080/uploadurl
	// on your dev deployment
	UploadUrl string
}

// OneFile expects an absolute filename
// and will POST that to the nometic server
func OneFile(filename string, server Server) error {
	queryUrl := fmt.Sprintf("%s?user_email=%s&password=%s", server.UploadUrl, server.UserEmail, server.UserPassword)
	body, err := getUrlBody(queryUrl)
	if err != nil {
		return err
	}

	return upload(filename, server.UserEmail, server.UserPassword, body)
}

func getUrlBody(url string) (string, error) {
	res, err := http.Get(url)
	if err != nil {
		return "", err
	}
	robots, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	return string(robots), err
}

// Upload POSTs form with these fields to url
func upload(filepath, userEmail, password, url string) (err error) {
	// code from Mike Rosset 
	// https://gist.github.com/f469a3246c514a3fbb7c
	// adapted by Patrick Borgeest (pretty much just changed the names of the fields)

	// Create buffer
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)

	// Create a form field writer for field user_email
	emailField, err := w.CreateFormField("user_email")
	if err != nil {
		return err
	}
	emailField.Write([]byte(userEmail))

	// Create a form field writer for field password
	passwordField, err := w.CreateFormField("password")
	if err != nil {
		return err
	}
	passwordField.Write([]byte(password))

	// Create file field
	filename := getFilenameFromFilepath(filepath)
	fw, err := w.CreateFormFile("file", filename)
	if err != nil {
		return err
	}
	fd, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer fd.Close()
	// Write file field from file to upload
	_, err = io.Copy(fw, fd)
	if err != nil {
		return err
	}

	// Important if you do not close the multipart writer you will not have a 
	// terminating boundry 
	w.Close()
	req, err := http.NewRequest("POST", url, buf)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}

	io.Copy(os.Stderr, res.Body) // Replace this with Status.Code check
	return err
}

func getFilenameFromFilepath(filepath string) (filename string) {
	split := strings.Split(filepath, "/")
	return split[len(split)-1]
}
