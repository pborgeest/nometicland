module bitbucket.org/pborgeest/nometicland/appengine

go 1.14

require (
	golang.org/x/net v0.0.0-20201022231255-08b38378de70
	google.golang.org/appengine v1.6.7
)
