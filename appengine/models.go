/*
Copyright 2013 Patrick Borgeest

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package appengine

import (
	"fmt"
	gae "google.golang.org/appengine"
	"google.golang.org/appengine/blobstore"
	"google.golang.org/appengine/datastore"
	"strings"
	"time"
)

const (
	// the format in which all dates are displayed and entered
	DateformatYyyyMmDd = "2006-01-02"
)


// UserInfo represents the metadata associated with the Google User
// currently logged-in to the app
type UserInfo struct {
	// User stores the email address of the currently logged-in user
	// this is used as the primary key
	User string 

	// MediaObjects is a count of the MediaObjects currently associated with this user
	MediaObjects int64 

	// UploadPassword is a plain-text string that protects the scan upload API
	UploadPassword string 
}

// MediaObject represents the metadata associated with each individual uploaded scan
type MediaObject struct {
	// Owner is the key of the UserInfo of the user that uploaded the file
	Owner *datastore.Key 

	// IntID is the entity ID of the key associated with this MediaObject struct
	// Not stored in datastore but filled on each get()
	IntID int64 `datastore:"-"`

	// Blob is the key of blobstore entry with this uploaded file
	Blob gae.BlobKey 

	// Creation the time when this struct was originally created
	Creation time.Time 

	// ContentType is the MIME-type of the uploaded file.
	// As the mime/multipart package does not detect Content-Type
	// before sending the file in the command line client, this is
	// detected in the webapp and so this field may differ from the
	// content-type for the associated blob in the blobstore
	ContentType string 

	// Filename is the name of the file when it was uploaded
	Filename string 

	// Size in bytes of the uploaded file
	Size int64

	// Document is the key of the associated Document struct.
	// A Document has many MediaObjects. When newly uploaded,
	// a MediaObject is not associated with a Document.
	Document *datastore.Key

	// LacksDocument is false when this MediaObject is associated with a Document.
	// When newly uploaded, a MediaObject is not associated with a Document.
	LacksDocument bool
}

// NewMediaObject is a constructor for the MediaObject struct
func NewMediaObject(userKey *datastore.Key, uploadFile *blobstore.BlobInfo, contentType string) *MediaObject {
	return &MediaObject{
		Owner:         userKey,
		Blob:          uploadFile.BlobKey,
		Creation:      uploadFile.CreationTime,
		ContentType:   contentType,
		Filename:      uploadFile.Filename,
		Size:          uploadFile.Size,
		LacksDocument: true,
	}
}

// UrlResize returns the URL that displays this struct at an unspecified size.
// The size must by subsequently specified by concatenating an integer to make a legal URL.
func (mo *MediaObject) UrlResize() string {
	return fmt.Sprintf("%s?resize=", mo.displayUrl())
}

// ThumbUrl returns the URL that displays this struct at a thumbnail size.
func (mo *MediaObject) ThumbUrl() string {
	return fmt.Sprintf("%s?resize=300", mo.displayUrl())
}

// displayUrl returns the URL that displays this struct at its original size
func (mo *MediaObject) displayUrl() string {
	return fmt.Sprintf("/resource/%d/%s", mo.IntID, mo.Filename)
}

// MediaObjectVM stores the MediaObject data required by the view templates
type MediaObjectVM struct {
	Id        int64
	UrlResize string
	ThumbUrl  string
}

// MakeViewModel returns a new DocumentVM with the data from this struct
func (mo *MediaObject) MakeViewModel() MediaObjectVM {
	return MediaObjectVM{
		Id:        mo.IntID,
		UrlResize: mo.UrlResize(),
		ThumbUrl:  mo.ThumbUrl(),
	}
}

// MakeMediaObjectViewModels takes a slice of MediaObjects and returns a slice of
// the same number of MediaObjectVMs with the data converted.
func MakeMediaObjectViewModels(mediaObjects []MediaObject) []MediaObjectVM {
	models := make([]MediaObjectVM, len(mediaObjects))
	for i := 0; i < len(mediaObjects); i++ {
		models[i] = mediaObjects[i].MakeViewModel()
	}
	return models
}

// Document is a structure that groups scans into a logical unit.
// A letter (Stored as a document) could have several pages
// (each is a MediaObject), for example
type Document struct {
	// Owner is the key of the UserInfo of the user that created the Document
	Owner *datastore.Key

	// Pages are the keys of each Media Object that contitute this Document
	Pages []*datastore.Key

	// IntID is the entity ID of the key associated with this Document struct
	// Not stored in datastore but filled on each get()
	IntID int64 `datastore:"-"`

	// DocDate is the user-nominated date associated with this document. It can
	// store any date the user likes but is intended to be when the document was
	// received, or, perhaps, written or sent
	DocDate time.Time 

	// NoDate is false when DocDate has been set by the user
	NoDate bool

	// Creation is the date the Document struct was created
	Creation time.Time

	// Title is the user-nominated title of the document
	Title string

	// Description is the user-nominated description of the document
	Description string

	// Tags is the slice of zero or more tags associated with the document by the user
	Tags SeparatedString /* ScanCab: `datastore:"tags"` */

	// LowercaseTags is the content of Tags but stored lowercase as a
	// canonical version so searches on tags can be case-insensitive
	LowercaseTags SeparatedString

	// NoTags is true when Tags is empty
	NoTags bool

	// PhysicalLocation is the user-nominated description of the location
	// of the physical document of which the MediaObjects associated with this
	PhysicalLocation string

	// DueDate is the user-nominated date that the document is "due". The
	// meaning of what "due" means in relation to each particular document
	// is up to the user
	DueDate time.Time
}

// NewDocument is a constructor for the Document struct
func NewDocument(userKey *datastore.Key, pages []*datastore.Key) *Document {
	return &Document{
		Owner:    userKey,
		Pages:    pages,
		NoDate:   true,
		Creation: time.Now(),
		NoTags:   true,
	}
}

// SetTags takes a comma-separated string object that is a list of tags
// and both stores them in the Tags field and stores them in the
// LowercaseTags field after forcing each element lowercase.
// It determines if commaSeparatedTags actually has any tags and
// sets NoTags on the target appropriately
func (doc *Document) SetTags(commaSeparatedTags SeparatedString) {
	doc.Tags = commaSeparatedTags
	doc.LowercaseTags = commaSeparatedTags.Map(func(s string) string {
		return strings.ToLower(s)
	})
	doc.NoTags = commaSeparatedTags.IsEmpty()
}

// DisplayUrl returns the url that displays this struct
func (doc *Document) DisplayUrl() string {
	return fmt.Sprintf("/doc/%d", doc.IntID)
}

// SomeTitle returns this struct's title or, failing that, its tags -
// and even failing that, its IntID
func (doc *Document) SomeTitle() string {
	if doc.Title != "" {
		return doc.Title
	}
	if doc.NoTags {
		return fmt.Sprintf("Docid %d", doc.IntID)
	}
	return strings.Join(doc.Tags, ",")
}

// DateYyyyMmDd formats this struct's DocDate according to the DateformatYyyyMmDd const
func (doc *Document) DateYyyyMmDd() string {
	return formatYyyyMmDd(doc.DocDate)
}

// DateYyyyMmDd formats this struct's DueDate according to the DateformatYyyyMmDd const
func (doc *Document) DueYyyyMmDd() string {
	return formatYyyyMmDd(doc.DueDate)
}

// formatYyyyMmDd is a convenience function that formats a given Time according to
// the DateformatYyyyMmDd const
func formatYyyyMmDd(indate time.Time) string {
	if indate.IsZero() {
		return ""
	}
	return indate.Format(DateformatYyyyMmDd)
}

// DocumentVM stores the Document data required by the view templates
type DocumentVM struct {
	Id                int64
	Title             string
	SomeTitle         string
	Description       string
	DateYyyyMmDd      string
	DueYyyyMmDd       string
	DisplayUrl        string
	TagCommaSeparated string
	PhysicalLocation  string
}

// MakeViewModel returns a new DocumentVM with the data from this struct
func (doc *Document) MakeViewModel() DocumentVM {
	return DocumentVM{
		Id:                doc.IntID,
		DisplayUrl:        doc.DisplayUrl(),
		Title:             doc.Title,
		SomeTitle:         doc.SomeTitle(),
		Description:       doc.Description,
		DateYyyyMmDd:      doc.DateYyyyMmDd(),
		DueYyyyMmDd:       doc.DueYyyyMmDd(),
		TagCommaSeparated: doc.Tags.String(),
		PhysicalLocation:  doc.PhysicalLocation,
	}
}

// MakeDocumentViewModels takes a slice of Documents and returns a slice of
// the same number of DocumentVMs with the data converted.
func MakeDocumentViewModels(docs []*Document) []DocumentVM {
	models := make([]DocumentVM, len(docs))
	for i := 0; i < len(docs); i++ {
		models[i] = docs[i].MakeViewModel()
	}
	return models
}

type KeyStorer interface {
	StoreKey(key *datastore.Key)
}

func (mo *MediaObject) StoreKey(key *datastore.Key) {
	mo.IntID = key.IntID()
}

func (doc *Document) StoreKey(key *datastore.Key) {
	doc.IntID = key.IntID()
}
