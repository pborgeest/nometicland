
package main

import (
	"log"
	"net/http"
	"os"
	nom "bitbucket.org/pborgeest/nometicland/appengine"
)


func handlers() {
	http.HandleFunc("/", nom.HandleRoot)
	http.HandleFunc("/serve/", nom.HandleServe)
	http.HandleFunc("/upload", nom.HandleUpload)
	http.HandleFunc("/resource/", nom.HandleResource)
	http.HandleFunc("/makedoc", nom.HandleMakedoc)
	http.HandleFunc("/changedoc", nom.HandleChangedoc)
	http.HandleFunc("/uploadurl", nom.HandleUploadUrl)
	http.HandleFunc("/doc/", nom.HandleDoc)
	http.HandleFunc("/hello", nom.HandleHello)
	http.HandleFunc("/robots.txt", nom.HandleRobots)
}
func main() {
    handlers()
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}

	log.Printf("Listening on port %s", port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
	}

}
/*
func indexHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	fmt.Fprint(w, "Hello, World!")
}
*/
