/*
Copyright 2013 Patrick Borgeest

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

// Comma handles the parsing and formatting of a string of tags separated by commas
package appengine

import (
	"strings"
)

type SeparatedString []string

// New returns a newly created SeparatedString value from the string given.
// There is no error if the string does not include a comma.
func NewSeparatedString(commaSeparated string) SeparatedString {
	split := strings.Split(commaSeparated, ",")
	for i, tag := range split {
		split[i] = strings.TrimSpace(tag)
	}
	return split
}

// String formats the string in the canonical comma-separated manner
func (css SeparatedString) String() string {
	return strings.Join(css, ", ")
}

// Map returns a new SeparatedString that results from running the function
// f() on each string member of the SeparatedString that calls this function
func (css SeparatedString) Map(f func(string) string) SeparatedString {
	output := make(SeparatedString, len(css))
	for i, el := range css {
		output[i] = f(el)
	}
	return output
}

func (css SeparatedString) IsEmpty() bool {
	if len(css) == 0 {
		return true
	}
	if len(css) == 1 && css[0] == "" {
		return true
	}
	return false
}
